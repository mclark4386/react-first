import React from 'react';

class ContactForm extends React.Component {
    constructor(){
	super();
	this.state = {
	    name: '',
	    phone: ''
	};
    }

    _handleChange(ev){
	var hash = {}
	hash[ev.target.name] = ev.target.value
	this.setState(hash);
    }

    _handleSubmit(ev){
	ev.preventDefault();
	ev.stopPropagation();
	this.props.handleAdd(this.state);
	this.setState({name: '', phone: ''});
    }
    
    render(){
	return (
		<form onSubmit={this._handleSubmit.bind(this)} className='form-inline' >
		<input type='text' name='name' value={this.state.name} onChange={this._handleChange.bind(this)} className='form-control' placeholder="Name"/>
		<input type='phone' name='phone' value={this.state.phone} onChange={this._handleChange.bind(this)} className='form-control' placeholder="Phone"/>
		<input type='submit' className='form-control'  />
	    </form>
	)
    }
}

export default ContactForm;
