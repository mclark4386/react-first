import React from 'react';
import Contact from './Contact';
import ContactForm from './ContactForm';

class ContactsList extends React.Component {
    constructor(props){
	super(props);
	this.state = {
	    search: '',
	    contacts: props.contacts
	};
    }

    _handleSearch(event){
	this.setState({search: event.target.value.substr(0,20)});
    }

    _handleAdd(contact){
	var contacts = this.state.contacts.slice();
	contacts.push(contact);
	this.setState({contacts: contacts});
    }
    
    render(){
	let filteredContacts = this.state.contacts.filter((contact)=>{
	    return contact.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1
	});
	return (
	    <div>
		<input type="text" value={this.state.search} onChange={this._handleSearch.bind(this)}/>
		<ul>
		{filteredContacts.map((contact)=> {
		    return <Contact {...contact} key={contact.phone} />
		    })}
	    </ul>
		<ContactForm handleAdd={this._handleAdd.bind(this)} />
		</div>
	)
    }
}

export default ContactsList;
