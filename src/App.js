import React from 'react';
import ContactsList from './ContactsList';

let contacts = [{
    name: 'bob',
    phone: '12341234'
},{
    name: 'janet',
    phone: '123415234'
}];

class App extends React.Component {
    render(){
	return (
		<div>
		<h1>Testing dis</h1>
		<ContactsList contacts={this.props.contacts} />
	    </div>
	)
    }
}

React.render(<App contacts={contacts} />, document.getElementById('app'));
